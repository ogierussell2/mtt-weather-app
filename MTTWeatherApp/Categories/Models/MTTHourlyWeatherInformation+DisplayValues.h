//
//  MTTHourlyWeatherInformation+DisplayValues.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 05/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTHourlyWeatherInformation.h"

@interface MTTHourlyWeatherInformation (DisplayValues)

- (NSString *)displayNameForProperty:(NSString *)property;
- (NSString *)displayValueForProperty:(NSString *)property;

@end
