//
//  MTTHourlyWeatherInformation+DisplayValues.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 05/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTHourlyWeatherInformation+DisplayValues.h"

@implementation MTTHourlyWeatherInformation (DisplayValues)

#pragma mark - Display Names for Properties

- (NSString *)displayNameForProperty:(NSString *)property {
    
    if ([property isEqualToString:NSStringFromSelector(@selector(chanceoffog))]) {
        return @"Fog Chance";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceoffrost))]) {
        return @"Frost Chance";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofhightemp))]) {
        return @"High Temp Chance";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofovercast))]) {
        return @"Overcast Chance";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofrain))]) {
        return @"Rain Chance";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofremdry))]) {
        return @"Dry Chance";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofsnow))]) {
        return @"Snow Chance";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofsunshine))]) {
        return @"Sunshine Chance";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofthunder))]) {
        return @"Thunder Chance";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofwindy))]) {
        return @"Windy Chance";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(DewPointF)) ]) {
        return @"Dew Point";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(HeatIndexC))] || [property isEqualToString:NSStringFromSelector(@selector(HeatIndexF))]) {
        return @"Heat Index";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(tempC))] || [property isEqualToString:NSStringFromSelector(@selector(tempF))]) {
        return @"Temperature";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(visibility))]) {
        return @"Visibility";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(weatherCode))]) {
       return @"Weather Code";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(cloudcover))]) {
        return @"Cloudcover";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(FeelsLikeC))] || [property isEqualToString:NSStringFromSelector(@selector(FeelsLikeF))]) {
        return @"Feels Like";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(humidity))]) {
        return @"Humidity";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(precipMM))]) {
        return @"Precipitation";
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(pressure))]) {
        return @"Pressure";
    }
    
    return property;
}

#pragma mark - Display Values for Properties

- (NSString *)displayValueForProperty:(NSString *)property {

    NSNumber *number = (NSNumber *)[self valueForKey:property];

    if ([property isEqualToString:NSStringFromSelector(@selector(chanceoffog))]) {
        return [self precentageValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceoffrost))]) {
        return [self precentageValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofhightemp))]) {
        return [self precentageValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofovercast))]) {
        return [self precentageValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofrain))]) {
        return [self precentageValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofremdry))]) {
        return [self precentageValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofsnow))]) {
        return [self precentageValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofsunshine))]) {
        return [self precentageValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofthunder))]) {
        return [self precentageValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(chanceofwindy))]) {
        return [self precentageValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(DewPointF)) ]) {
        return [self ferenheitValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(HeatIndexC))]) {
        return [self celsiusValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(HeatIndexF))]) {
        return [self ferenheitValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(HeatIndexC))]) {
        return [self celsiusValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(tempC))]) {
        return [self celsiusValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(tempF))]) {
        return [self ferenheitValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(FeelsLikeC))]) {
        return [self celsiusValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(FeelsLikeF))]) {
        return [self ferenheitValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(precipMM))]) {
        return [self millimeterValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(pressure))]) {
        return [self millibarsValueForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(visibility))]) {
        return [self kilometersValurForNumber:number];
    }
    else if ([property isEqualToString:NSStringFromSelector(@selector(humidity))]) {
        return [self precentageValueForNumber:number];
    }
    
    return [NSString stringWithFormat:@"%ld", number.longValue];
}

#pragma mark - Display Types

- (NSString *)celsiusValueForNumber:(NSNumber *)number {
    return [NSString stringWithFormat:@"%ld\u00B0C", number.longValue];
}

- (NSString *)ferenheitValueForNumber:(NSNumber *)number {
    return [NSString stringWithFormat:@"%ld\u00B0F", number.longValue];
}

- (NSString *)precentageValueForNumber:(NSNumber *)number {
    return [NSString stringWithFormat:@"%ld%%", number.longValue];
}

- (NSString *)millimeterValueForNumber:(NSNumber *)number {
    return [NSString stringWithFormat:@"%ld mm", number.longValue];
}

- (NSString *)millibarsValueForNumber:(NSNumber *)number {
    return [NSString stringWithFormat:@"%ld mb", number.longValue];
}

- (NSString *)kilometersValurForNumber:(NSNumber *)number {
    return [NSString stringWithFormat:@"%ld km", number.longValue];
}

@end
