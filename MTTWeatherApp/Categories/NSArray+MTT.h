//
//  NSArray+MTT.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (MTT)

- (id)safeObjectAtIndex:(NSInteger)index;
- (BOOL)canSafelyRemoveObjectAtIndex:(NSInteger)index;

@end
