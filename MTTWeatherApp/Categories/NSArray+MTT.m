//
//  NSArray+MTT.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "NSArray+MTT.h"

@implementation NSArray (MTT)

- (id)safeObjectAtIndex:(NSInteger)index {
    
    if (index <= self.count - 1 || (index == 0 && self.count == 1)) {
        return [self objectAtIndex:index];
    }
    
    return nil;
}

- (BOOL)canSafelyRemoveObjectAtIndex:(NSInteger)index {
    
    if (index < self.count) {
        return YES;
    }
    
    return NO;
}

@end
