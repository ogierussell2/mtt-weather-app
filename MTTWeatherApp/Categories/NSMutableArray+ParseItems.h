//
//  NSMutableArray+ParseItems.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (ParseItems)

- (NSMutableArray *)parseItemsToClass:(Class)itemClass;

@end
