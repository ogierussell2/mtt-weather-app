//
//  NSMutableArray+ParseItems.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "NSMutableArray+ParseItems.h"
#import "MJExtension.h"

@implementation NSMutableArray (ParseItems)

- (NSMutableArray *)parseItemsToClass:(Class)itemClass {
    
    if (self.count > 0) {
        [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            id object = [itemClass objectWithKeyValues:obj];
            [self replaceObjectAtIndex:idx withObject:object];
        }];
    }
    
    return self;
}

@end
