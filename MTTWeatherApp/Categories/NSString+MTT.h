//
//  NSString+MTT.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MTT)

- (NSArray *)stringSplitByCommas;

@end
