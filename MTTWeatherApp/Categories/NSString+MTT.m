//
//  NSString+MTT.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "NSString+MTT.h"

@implementation NSString (MTT)

- (NSArray *)stringSplitByCommas {

    NSString *noWhitespaces = [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSArray *components = [noWhitespaces componentsSeparatedByString:@","];
    return components;
}

@end
