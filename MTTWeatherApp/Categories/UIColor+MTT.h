//
//  UIColor+MTT.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MTT)

+ (UIColor *)mttDarkNavyBackgroundColor;
+ (UIColor *)mttLightGrayColor;

@end
