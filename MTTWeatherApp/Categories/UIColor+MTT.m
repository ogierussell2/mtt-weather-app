//
//  UIColor+MTT.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "UIColor+MTT.h"

@implementation UIColor (MTT)

+ (UIColor *)mttDarkNavyBackgroundColor {
    return [UIColor colorWithRed:0.09 green:0.19 blue:0.26 alpha:1];
}

+ (UIColor *)mttLightGrayColor {
    return [UIColor colorWithRed:0.93 green:0.91 blue:0.9 alpha:1];
}

@end
