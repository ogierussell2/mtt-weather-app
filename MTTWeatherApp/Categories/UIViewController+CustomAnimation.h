//
//  UIViewController+CustomAnimation.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 05/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (CustomAnimation)

- (void)showAddCity;

@end
