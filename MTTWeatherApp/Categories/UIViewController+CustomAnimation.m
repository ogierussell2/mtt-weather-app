//
//  UIViewController+CustomAnimation.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 05/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "UIViewController+CustomAnimation.h"
#import "MTTAddCityViewController.h"
#import "MTTFadeTransitionManager.h"

@interface UIViewController () <UIViewControllerTransitioningDelegate, MTTAddCityDelegate>

@end

@implementation UIViewController (CustomAnimation)

#pragma mark - Search Screen

- (void)showAddCity {
    MTTAddCityViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(MTTAddCityViewController.class)];
    controller.transitioningDelegate = self;
    controller.modalPresentationStyle = UIModalPresentationCustom;
    controller.view.backgroundColor = [UIColor clearColor];
    [controller setDelegate:self];
    
    [self presentViewController:controller animated:YES completion:^{}];
}

#pragma mark - UIViewControllerTransitionDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source {
    MTTFadeTransitionManager *transitionManager = [[MTTFadeTransitionManager alloc] init];
    transitionManager.presenting = YES;
    return transitionManager;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    MTTFadeTransitionManager *transitionManager = [[MTTFadeTransitionManager alloc] init];
    transitionManager.presenting = NO;
    return transitionManager;
}

@end
