//
//  MTTHourlyDetailCollectionViewCell.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTCollectionViewCell.h"

// models
#import "MTTHourlyWeatherInformation.h"

@interface MTTHourlyDetailCollectionViewCell : MTTCollectionViewCell

- (void)setDataWithProperty:(NSString *)property withValue:(id)value forObject:(MTTHourlyWeatherInformation *)hourlyInfo;

@end
