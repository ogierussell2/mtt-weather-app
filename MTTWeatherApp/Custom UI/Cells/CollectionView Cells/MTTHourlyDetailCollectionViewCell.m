//
//  MTTHourlyDetailCollectionViewCell.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTHourlyDetailCollectionViewCell.h"

// categories
#import "MTTHourlyWeatherInformation+DisplayValues.h"

@interface MTTHourlyDetailCollectionViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *propertyLabel;
@property (nonatomic, weak) IBOutlet UILabel *valueLabel;


@end

@implementation MTTHourlyDetailCollectionViewCell

- (void)setDataWithProperty:(NSString *)property withValue:(id)value forObject:(MTTHourlyWeatherInformation *)hourlyInfo {
    
    self.propertyLabel.text = [hourlyInfo displayNameForProperty:property];
    self.valueLabel.text = [hourlyInfo displayValueForProperty:property];
}

@end
