//
//  MTTCityTableViewCell.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
#import "MTTTableViewCell.h"

// models
#import "MTTCityData.h"

@interface MTTCityTableViewCell : MTTTableViewCell

- (void)setData:(MTTCityData *)cityData;
- (void)setImage:(UIImage *)cityImage;

@end
