//
//  MTTCityTableViewCell.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTCityTableViewCell.h"
#import "UIImageView+AFNetworking.h"

// models
#import "MTTWeatherResponse.h"

@interface MTTCityTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *cityNameLabel;
@property (nonatomic, weak) IBOutlet UIImageView *cityBackgroundImageView;
@property (nonatomic, weak) IBOutlet UILabel *degreesLabel;
@property (nonatomic, weak) IBOutlet UILabel *weatherDescriptionLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;

@end

@implementation MTTCityTableViewCell

#pragma mark - Setup

- (void)setData:(MTTCityData *)cityData {
    
    MTTWeatherResponse *weatherData = cityData.weatherData;
    
    if (weatherData) {
        self.degreesLabel.text = [NSString stringWithFormat:@"%@\u00B0", weatherData.currentCondition.temp_C];
        self.weatherDescriptionLabel.text = weatherData.currentCondition.weatherDesc;
        self.timeLabel.text = weatherData.currentCondition.observation_time;
    }
    
    self.cityNameLabel.text = cityData.city;
}

- (void)setImage:(UIImage *)cityImage {
    self.cityBackgroundImageView.image = cityImage;
}

#pragma mark - Class Methods

+ (CGFloat)height {
    return 150.0f;
}

#pragma mark - End of Life

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.cityNameLabel.text = @"";
    self.weatherDescriptionLabel.text = @"";
    self.degreesLabel.text = @"";
}

@end
