//
//  MTTCurrentDayForecastCollectionViewCell.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTTableViewCell.h"
#import "MTTDailyWeatherInformation.h"

@interface MTTCurrentDayForecastTableViewCell : MTTTableViewCell

- (void)setData:(MTTDailyWeatherInformation *)dailyData forCity:(NSString *)city;

@end
