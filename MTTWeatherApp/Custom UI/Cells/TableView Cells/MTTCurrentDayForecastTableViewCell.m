//
//  MTTCurrentDayForecastCollectionViewCell.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTCurrentDayForecastTableViewCell.h"
#import "MTTDateFormatter.h"

@interface MTTCurrentDayForecastTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *weatherDescriptionLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *locationLabel;
@property (nonatomic, weak) IBOutlet UILabel *temperatureLabel;


@end

@implementation MTTCurrentDayForecastTableViewCell

- (void)setData:(MTTDailyWeatherInformation *)dailyData forCity:(NSString *)city {
    
    NSDictionary *minMax = [dailyData minimumAndMaximumTemperatures];
    self.temperatureLabel.text = [NSString stringWithFormat:@"%ld\u00B0 %ld\u00B0", (long)[[minMax objectForKey:kMinimumTemperature] integerValue], (long)[[minMax objectForKey:kMaximumTemperature] integerValue]];
    self.dateLabel.text = [MTTDateFormatter dayWithDateForString:dailyData.date];
    self.locationLabel.text = city;
    self.weatherDescriptionLabel.text = [dailyData middleOfDayWeatherDescription];
    
}

+ (CGFloat)height {
    return 200.0f;
}

@end
