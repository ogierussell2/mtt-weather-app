//
//  MTTDetailedDataTableViewCell.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
#import "MTTTableViewCell.h"

// models
#import "MTTDailyWeatherInformation.h"

@interface MTTDetailedDataTableViewCell : MTTTableViewCell

- (void)setData:(MTTDailyWeatherInformation *)weatherInformation;

@end
