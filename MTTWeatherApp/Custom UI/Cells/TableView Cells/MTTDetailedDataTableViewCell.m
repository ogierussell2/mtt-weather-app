//
//  MTTDetailedDataTableViewCell.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTDetailedDataTableViewCell.h"
#import <objc/runtime.h>

// collectionView
#import "MTTForecastDetailCollectionViewDatasource.h"
#import "MTTForecastDetailCollectionViewDelegate.h"
#import "MTTHourlyWeatherInformation.h"

// delegate
#import "MTTScrolledSectionDelegate.h"

// utilities
#import "MTTRuntimePropertiesForClass.h"

// cells
#import "MTTHourlyDetailCollectionViewCell.h"

// categories
#import "NSArray+MTT.h"

@interface MTTDetailedDataTableViewCell () <MTTScrolledSectionDelegate>

@property (nonatomic, strong) MTTForecastDetailCollectionViewDatasource *datasource;
@property (nonatomic, strong) MTTForecastDetailCollectionViewDelegate *delegate;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, strong) NSArray *hourlyData;
@property (nonatomic, assign) NSInteger currentSection;

@end

@implementation MTTDetailedDataTableViewCell

#pragma mark - Setup

- (void)setData:(MTTDailyWeatherInformation *)weatherInformation {
    
    self.hourlyData = [NSArray arrayWithArray:weatherInformation.hourly];
    MTTHourlyWeatherInformation *firstHour = [weatherInformation.hourly firstObject];
    self.timeLabel.text = [firstHour hourStringForHour];
    
    NSArray *properties = [MTTHourlyWeatherInformation runtimeProperties];
    [self registerCells];
    [self setupDatasourceWithProperties:properties withHourlyInformation:weatherInformation.hourly];
    [self setupDelegate];
    [self.collectionView reloadData];
}

- (void)setupDatasourceWithProperties:(NSArray *)properties withHourlyInformation:(NSArray *)hourlyInformation {
    self.datasource = [[MTTForecastDetailCollectionViewDatasource alloc] init];
    [self.datasource setData:hourlyInformation forProperties:properties];
    self.collectionView.dataSource = self.datasource;
}

- (void)setupDelegate {
    self.delegate = [[MTTForecastDetailCollectionViewDelegate alloc] init];
    [self.delegate setScrolledSectionDelegate:self];
    self.collectionView.delegate = self.delegate;
}

#pragma mark - Helpers

- (void)registerCells {
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass(MTTHourlyDetailCollectionViewCell.class) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass(MTTHourlyDetailCollectionViewCell.class)];
}

#pragma mark - Scrolled Section Delegate

- (void)scrollView:(UIScrollView *)scrollView didScrollToSection:(NSInteger)section {
    
    MTTHourlyWeatherInformation *hourly = [self.hourlyData safeObjectAtIndex:section];
    
    if (hourly) {
        self.currentSection = section;
        self.timeLabel.text = self.timeLabel.text = [hourly hourStringForHour];
    }
}

#pragma mark - IBActions

- (IBAction)previousSection:(id)sender {
    
    if (self.currentSection != 0) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:self.currentSection - 1] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    }
    else {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    }
}

- (IBAction)nextSection:(id)sender {
    if (self.currentSection != self.hourlyData.count - 1) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:self.currentSection + 1] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    }
}


#pragma mark - End of Life

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.currentSection = 0;
    [self.delegate setScrolledSectionDelegate:nil];
    [self.collectionView setContentOffset:CGPointMake(0.0f, 0.0f)];
}

@end

