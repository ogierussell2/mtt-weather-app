//
//  MTTForecastTableViewCell.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTForecastTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "MTTDateFormatter.h"

// categories
#import "UIColor+MTT.h"

@interface MTTForecastTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *weatherDescription;
@property (nonatomic, weak) IBOutlet UIImageView *weatherImage;
@property (nonatomic, weak) IBOutlet UILabel *degreesLabel;

@end

@implementation MTTForecastTableViewCell

- (void)setData:(MTTDailyWeatherInformation *)dailyData {
    
    self.contentView.backgroundColor = [UIColor mttLightGrayColor];
    NSDictionary *minMax = [dailyData minimumAndMaximumTemperatures];
    self.degreesLabel.text = [NSString stringWithFormat:@"%ld\u00B0 %ld\u00B0", (long)[[minMax objectForKey:kMinimumTemperature] integerValue], (long)[[minMax objectForKey:kMaximumTemperature] integerValue]];
    self.weatherDescription.text = [MTTDateFormatter dayWithDateForString:dailyData.date];
    [self.weatherImage setImageWithURL:[NSURL URLWithString:[dailyData middleOfDayWeatherIcon]]];
}

+ (CGFloat)height {
    return 80.0f;
}

@end
