//
//  MTTCollectionViewCell.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;

@interface MTTCollectionViewCell : UICollectionViewCell

+ (CGSize)size;

@end
