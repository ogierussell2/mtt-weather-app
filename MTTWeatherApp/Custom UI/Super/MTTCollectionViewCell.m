//
//  MTTCollectionViewCell.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTCollectionViewCell.h"

@implementation MTTCollectionViewCell

+ (CGSize)size {
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 100.0f);
}

@end
