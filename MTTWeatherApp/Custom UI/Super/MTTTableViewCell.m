//
//  MTTTableViewCell.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTTableViewCell.h"

@implementation MTTTableViewCell

+ (CGFloat)height {
    // default value
    return 100.0f;
}

@end
