//
//  MTTCitiesDatasource.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import Foundation;

// models
#import "MTTCityData.h"

#import "MTTDeleteRowDelegate.h"

@interface MTTCitiesTableViewDatasource : NSObject

#pragma mark - Data

- (void)setData:(NSArray *)data forTableView:(UITableView *)tableView;
- (void)addDataToStartOfArray:(MTTCityData *)cityData;
- (MTTCityData *)objectForIndexPath:(NSIndexPath *)indexPath;
- (void)fetchDataWithCompletionBlock:(void (^)())completionBlock;
- (NSInteger)numberOfItems;

- (void)setDeleteRowDelegate:(id<MTTDeleteRowDelegate>)deleteRowDelegate;

@end
