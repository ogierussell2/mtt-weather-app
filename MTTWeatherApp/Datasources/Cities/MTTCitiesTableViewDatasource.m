//
//  MTTCitiesDatasource.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
#import "MTTCitiesTableViewDatasource.h"
#import "MTTAPIOperationManager.h"

// cells
#import "MTTCityTableViewCell.h"
#import "MTTSadPathTableViewCell.h"

// utilities
#import "MTTUserDefaults.h"

// categories
#import "NSArray+MTT.h"

// models
#import "MTTPlaceHolderWeatherObject.h"

@interface MTTCitiesTableViewDatasource () <UITableViewDataSource>

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) id<MTTDeleteRowDelegate>deleteRowDelegate;

@end

@implementation MTTCitiesTableViewDatasource

#pragma mark - Data

- (void)setData:(NSArray *)data forTableView:(UITableView *)tableView {
    
    if (data.count == 0) {
        _data = @[[[MTTPlaceHolderWeatherObject alloc] init]];
    }
    else {
        self.tableView = tableView;
        _data = data;
    }
}

- (void)addDataToStartOfArray:(MTTCityData *)cityData {
    
    if (!cityData) {
        return;
    }
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.data];
    
    if ([[array firstObject] isKindOfClass:[MTTPlaceHolderWeatherObject class]]) {
        [array removeObject:[array firstObject]];
    }
    
    [array insertObject:cityData atIndex:0];
    self.data = [NSArray arrayWithArray:array];
    [MTTUserDefaults addCity:cityData.city];
}

- (MTTCityData *)objectForIndexPath:(NSIndexPath *)indexPath {
    MTTCityData *cityData = [self.data safeObjectAtIndex:indexPath.row];
    return cityData;
}

- (NSInteger)numberOfItems {
    return self.data.count;
}

#pragma mark - Setup

- (void)setDeleteRowDelegate:(id<MTTDeleteRowDelegate>)deleteRowDelegate {
    _deleteRowDelegate = deleteRowDelegate;
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id object = [self.data objectAtIndex:indexPath.row];
    
    if ([object isKindOfClass:[MTTPlaceHolderWeatherObject class]]) {
        MTTSadPathTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(MTTSadPathTableViewCell.class)  forIndexPath:indexPath];
        return cell;
    }
    
    MTTCityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(MTTCityTableViewCell.class)  forIndexPath:indexPath];
    [cell setData:object];
    // temporary way to seperate out image, would ideally grab a specific city
    [cell setImage:[UIImage imageNamed:[NSString stringWithFormat:@"city%ld", (long)indexPath.row % 3]]];
    return cell;
}

#pragma mark - Helpers

- (NSInteger)indexForObjectMatchingCity:(NSString *)city {
    
    __block NSInteger index = -1;
    [self.data enumerateObjectsUsingBlock:^(MTTCityData *cityData, NSUInteger idx, BOOL *stop) {
        if ([city respondsToSelector:@selector(isEqualToString:)]) {
            if ([cityData.city caseInsensitiveCompare:city] == NSOrderedSame) {
                index = idx;
                *stop = YES;
            }
        }
    }];
    
    return index;
}

- (void)updateCellWithData:(MTTWeatherResponse *)weatherResponse {
    NSString *city = weatherResponse.request.city;
    __block NSInteger index = [self indexForObjectMatchingCity:city];
    
    MTTCityData *cityData = [self.data safeObjectAtIndex:index];
    cityData.weatherData = weatherResponse;
    
    if (index >= 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        if ([self.tableView cellForRowAtIndexPath:indexPath]) {
            MTTCityTableViewCell *cell = (MTTCityTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            [cell setData:cityData];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0 && [[self.data firstObject] isKindOfClass:[MTTPlaceHolderWeatherObject class]]) {
        return NO;
    }
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if ([self.data objectAtIndex:indexPath.row]) {
            NSMutableArray *mutableData = [NSMutableArray arrayWithArray:self.data];
            MTTCityData *cityData = [mutableData objectAtIndex:indexPath.row];
            [mutableData removeObject:cityData];
            [self setData:mutableData forTableView:tableView];
            
            if (mutableData.count == 0) {
                [tableView reloadData];
            }
            else {
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            [self.deleteRowDelegate tableView:tableView didDeleteCityData:cityData];
        }
    }
}

#pragma mark - API

- (void)fetchDataWithCompletionBlock:(void (^)())completionBlock {
    
    id firstObject = [self.data firstObject];
    
    if ([firstObject isKindOfClass:[MTTPlaceHolderWeatherObject class]]) {
        if (completionBlock) {
            completionBlock();
        }
        return;
    }
    
    [self.data enumerateObjectsUsingBlock:^(MTTCityData *cityData, NSUInteger idx, BOOL *stop) {
            MTTAPIOperationManager *manager = [[MTTAPIOperationManager alloc] init];
            
            [manager getWeatherForCity:cityData.city withCompletionBlock:^(id weatherData) {
                [self updateCellWithData:weatherData];
                if (completionBlock) {
                    completionBlock();
                }
            } withFailureBlock:^(NSError *error) {
                if (completionBlock) {
                    completionBlock();
                }
            }];
    }];
}

@end
