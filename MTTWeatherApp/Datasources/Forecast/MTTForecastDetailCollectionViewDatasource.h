//
//  MTTForecastDetailCollectionViewDatasource.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTForecastDetailCollectionViewDatasource : NSObject <UICollectionViewDataSource>

- (void)setData:(NSArray *)data forProperties:(NSArray *)properties;

@end
