//
//  MTTForecastDetailCollectionViewDatasource.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
#import "MTTForecastDetailCollectionViewDatasource.h"

// cells
#import "MTTHourlyDetailCollectionViewCell.h"

// data
#import "MTTHourlyWeatherInformation.h"

@interface MTTForecastDetailCollectionViewDatasource ()

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *properties;

@end

@implementation MTTForecastDetailCollectionViewDatasource

#pragma mark - Data

- (void)setData:(NSArray *)data forProperties:(NSArray *)properties {
    _data = data;
    self.properties = properties;
}

#pragma mark - CollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.data.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {    
    return self.properties.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MTTHourlyDetailCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(MTTHourlyDetailCollectionViewCell.class) forIndexPath:indexPath];
    MTTHourlyWeatherInformation *hourlyInformation = [self.data objectAtIndex:indexPath.section];
    NSString *propertyName = [self.properties objectAtIndex:indexPath.row];
    id value = [hourlyInformation valueForKey:propertyName];
    [cell setDataWithProperty:propertyName withValue:value forObject:hourlyInformation];
    
    return cell;
}

@end
