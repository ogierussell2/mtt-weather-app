//
//  MTTForecastCollectionViewDatasource.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import Foundation;

@interface MTTForecastTableViewDatasource : NSObject

#pragma mark - Data

- (void)setData:(NSArray *)data withCity:(NSString *)city;
- (BOOL)successfullyInsertPlaceholderForIndex:(NSInteger)index;
- (BOOL)successfullyRemovedPlaceHolderForIndex:(NSInteger)index;
- (BOOL)isPlaceholderObjectAtIndex:(NSInteger)index;

@end
