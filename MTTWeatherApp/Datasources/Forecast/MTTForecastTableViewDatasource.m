//
//  MTTForecastCollectionViewDatasource.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
#import "MTTForecastTableViewDatasource.h"

// cells
#import "MTTCurrentDayForecastTableViewCell.h"
#import "MTTForecastTableViewCell.h"
#import "MTTDetailedDataTableViewCell.h"

// categories
#import "NSArray+MTT.h"

// models
#import "MTTPlaceHolderWeatherObject.h"

@interface MTTForecastTableViewDatasource () <UITableViewDataSource>

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, strong) NSMutableArray *detailIndexes;

@end

@implementation MTTForecastTableViewDatasource

#pragma mark - Data

- (void)setData:(NSArray *)data withCity:(NSString *)city {
    _data = data;
    self.city = city;
}

- (BOOL)successfullyInsertPlaceholderForIndex:(NSInteger)index {
    MTTDailyWeatherInformation *dailyWeather = [self.data safeObjectAtIndex:index];
    
    if (!dailyWeather) {
        return NO;
    }
    
    NSMutableArray *mutableData = [NSMutableArray arrayWithArray:self.data];
    
    MTTPlaceHolderWeatherObject *placeHolder = [[MTTPlaceHolderWeatherObject alloc] init];
    
    if (index == mutableData.count - 1) {
        [mutableData addObject:placeHolder];
    }
    else {
        [mutableData insertObject:placeHolder atIndex:index + 1];
    }
    
    self.data = [NSArray arrayWithArray:mutableData];
    
    return YES;
}

- (BOOL)successfullyRemovedPlaceHolderForIndex:(NSInteger)index {
    
    NSMutableArray *mutableData = [NSMutableArray arrayWithArray:self.data];
    
    if ([mutableData canSafelyRemoveObjectAtIndex:index] && [[mutableData objectAtIndex:index] isKindOfClass:[MTTPlaceHolderWeatherObject class]]) {
        [mutableData removeObjectAtIndex:index];
        self.data = [NSArray arrayWithArray:mutableData];
        return YES;
    }
    
    return NO;
}

- (BOOL)isPlaceholderObjectAtIndex:(NSInteger)index {
    return [[self.data safeObjectAtIndex:index] isKindOfClass:[MTTPlaceHolderWeatherObject class]];
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        MTTCurrentDayForecastTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(MTTCurrentDayForecastTableViewCell.class) forIndexPath:indexPath];
        [cell setData:[self.data objectAtIndex:indexPath.row] forCity:self.city];
        return cell;
    }
    
    NSObject *object = [self.data objectAtIndex:indexPath.row];
    
    if ([object isKindOfClass:[MTTPlaceHolderWeatherObject class]]) {
        MTTDetailedDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(MTTDetailedDataTableViewCell.class) forIndexPath:indexPath];
        [cell setData:[self.data objectAtIndex:indexPath.row - 1]];
        return cell;
    }
    
    MTTForecastTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(MTTForecastTableViewCell.class) forIndexPath:indexPath];
    [cell setData:[self.data objectAtIndex:indexPath.row]];
    
    return cell;
}

@end
