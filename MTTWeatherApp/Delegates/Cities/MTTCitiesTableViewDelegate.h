//
//  MTTCitiesDelegate.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import Foundation;

// delegates
#import "MTTSelectedRowDelegate.h"

@interface MTTCitiesTableViewDelegate : NSObject

#pragma mark - Setup

- (void)setSelectedRowDelegate:(id<MTTSelectedRowDelegate>)delegate;

@property (nonatomic, copy) CGFloat(^heightForCellAtIndexPath)(NSIndexPath *indexPath);

@end
