//
//  MTTCitiesDelegate.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
#import "MTTCitiesTableViewDelegate.h"

// cells
#import "MTTCityTableViewCell.h"

@interface MTTCitiesTableViewDelegate () <UITableViewDelegate>

@property (nonatomic, assign) id<MTTSelectedRowDelegate>selectedRowDelegate;

@end

@implementation MTTCitiesTableViewDelegate

#pragma mark - Setup

- (void)setSelectedRowDelegate:(id<MTTSelectedRowDelegate>)delegate {
    _selectedRowDelegate = delegate;
}

#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.heightForCellAtIndexPath) {
        return self.heightForCellAtIndexPath(indexPath);
    }
    
    return [MTTCityTableViewCell height];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.selectedRowDelegate scrollView:tableView didSelectCellAtIndexPath:indexPath];
}

@end
