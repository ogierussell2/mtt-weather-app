//
//  MTTForecastDetailCollectionViewDelegate.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import Foundation;

// delegates
#import "MTTScrolledSectionDelegate.h"

@interface MTTForecastDetailCollectionViewDelegate : NSObject <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

#pragma mark - Setup

- (void)setScrolledSectionDelegate:(id<MTTScrolledSectionDelegate>)scrolledSectionDelegate;

@end
