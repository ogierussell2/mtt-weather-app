//
//  MTTForecastDetailCollectionViewDelegate.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 03/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
#import "MTTForecastDetailCollectionViewDelegate.h"

static CGFloat cellSize;

@interface MTTForecastDetailCollectionViewDelegate () <UIScrollViewDelegate>

@property (nonatomic, assign) id<MTTScrolledSectionDelegate> scrolledSectionDelegate;
@property (nonatomic, assign) NSInteger previousSection;

@end

@implementation MTTForecastDetailCollectionViewDelegate

#pragma mark - Setup

- (void)setScrolledSectionDelegate:(id<MTTScrolledSectionDelegate>)scrolledSectionDelegate {
    _scrolledSectionDelegate = scrolledSectionDelegate;
}

#pragma mark - CollectionView Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    cellSize = collectionView.frame.size.width / 1.5f;
    return CGSizeMake(cellSize, collectionView.frame.size.height);
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UICollectionView *)scrollView {

    CGFloat sectionWidth = cellSize * [scrollView numberOfItemsInSection:0];
    float fractionalSection = scrollView.contentOffset.x / sectionWidth + 0.001;
    NSInteger section = (NSInteger)fractionalSection;
    
    if (section != self.previousSection) {
        [self.scrolledSectionDelegate scrollView:scrollView didScrollToSection:section];
        self.previousSection = section;
    }
}

@end
