//
//  MTTForecastCollectionViewDelegate.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
#import "MTTForecastTableViewDelegate.h"

// cells
#import "MTTCurrentDayForecastTableViewCell.h"
#import "MTTForecastTableViewCell.h"

@interface MTTForecastTableViewDelegate () <UITableViewDelegate>

@property (nonatomic, assign) id<MTTSelectedRowDelegate>selectedRowDelegate;

@end

@implementation MTTForecastTableViewDelegate

#pragma mark - Setup

- (void)setSelectedRowDelegate:(id<MTTSelectedRowDelegate>)delegate {
    _selectedRowDelegate = delegate;
}

#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return [MTTCurrentDayForecastTableViewCell height];
    }
    
    return [MTTForecastTableViewCell height];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.selectedRowDelegate scrollView:tableView didSelectCellAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.selectedRowDelegate scrollView:tableView didDeselectCellAtIndexPath:indexPath];
}

@end
