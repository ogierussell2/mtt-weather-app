//
//  MTTAddCityDelegate.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@class MTTCityData;

@protocol MTTAddCityDelegate <NSObject>

- (void)addCityData:(MTTCityData *)cityData;

@end
