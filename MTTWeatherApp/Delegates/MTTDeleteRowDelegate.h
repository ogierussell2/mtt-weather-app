//
//  MTTDeleteRowDelegate.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
#import "MTTCityData.h"

@protocol MTTDeleteRowDelegate <NSObject>

- (void)tableView:(UITableView *)tableView didDeleteCityData:(MTTCityData *)cityData;

@end
