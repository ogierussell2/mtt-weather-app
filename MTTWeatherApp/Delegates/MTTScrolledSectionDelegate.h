//
//  MTTScrolledSectionDelegate.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import Foundation;

@protocol MTTScrolledSectionDelegate <NSObject>

- (void)scrollView:(UIScrollView *)scrollView didScrollToSection:(NSInteger)section;

@end
