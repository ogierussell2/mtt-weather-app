//
//  MTTSelectedRowDelegate.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;

@protocol MTTSelectedRowDelegate <NSObject>

- (void)scrollView:(UIScrollView *)scrollView didSelectCellAtIndexPath:(NSIndexPath *)indexPath;

@optional
- (void)scrollView:(UIScrollView *)scrollView didDeselectCellAtIndexPath:(NSIndexPath *)indexPath;

@end
