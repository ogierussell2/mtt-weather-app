//
//  MTTCityData.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import Foundation;
#import "MTTWeatherResponse.h"

@interface MTTCityData : NSObject

@property (nonatomic, copy) NSString *city;
@property (nonatomic, strong) MTTWeatherResponse *weatherData;

@end
