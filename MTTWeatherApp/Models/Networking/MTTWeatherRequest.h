//
//  MTTWeatherRequest.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTWeatherRequest : NSObject

#pragma mark - Properties

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *format;
@property (nonatomic, copy) NSString *numberOfDays;
@property (nonatomic, copy) NSString *apiKey;

#pragma mark - Initialization

- (instancetype)initWithCity:(NSString *)city;

#pragma mark - Helpers

- (NSDictionary *)dictionary;

@end
