//
//  MTTWeatherRequest.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTWeatherRequest.h"

@implementation MTTWeatherRequest

#pragma mark - Initialization

- (instancetype)initWithCity:(NSString *)city {
    self = [super init];
    
    if (self) {
        self.city = city;
        self.format = @"json";
        self.numberOfDays = @"5";
        self.apiKey = @"9b59ba0f40dfe3b6c90b5212223b7";
    }
    
    return self;
}

#pragma mark - Helpers

- (NSDictionary *)dictionary {
        
    if (!self.city) {
        self.city = @"Unknown";
    }
    
    return @{@"q": self.city, @"format": self.format, @"num_of_days": self.numberOfDays, @"key": self.apiKey};
}

@end
