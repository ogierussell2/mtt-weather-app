//
//  MTTWeatherResponse.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTTCurrentWeatherInformation.h"
#import "MTTRequest.h"

@interface MTTWeatherResponse : NSObject

@property (nonatomic, strong) MTTCurrentWeatherInformation *currentCondition;
@property (nonatomic, strong) MTTRequest *request;
@property (nonatomic, strong) NSArray *days;

- (instancetype)initWithData:(NSDictionary *)data;

@end
