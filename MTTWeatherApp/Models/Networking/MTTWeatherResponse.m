//
//  MTTWeatherResponse.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTWeatherResponse.h"
#import "MJExtension.h"
#import "NSMutableArray+ParseItems.h"
#import "MTTHourlyWeatherInformation.h"
#import "MTTDailyWeatherInformation.h"

@implementation MTTWeatherResponse

#pragma mark - Initialization

- (instancetype)initWithData:(NSDictionary *)data {
    self = [super init];
    
    if (self) {
        [self setValuesForKeysWithDictionary:[data objectForKey:NSStringFromSelector(@selector(data))]];
    }
    
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key {
    
    if ([key isEqualToString:@"current_condition"]) {
        NSDictionary *current = [(NSArray *)value firstObject];
        self.currentCondition = [MTTCurrentWeatherInformation objectWithKeyValues:current];
    }
    else if ([key isEqualToString:@"weather"]) {
        NSArray *days = (NSArray *)value;
        NSMutableArray *parsedDays = [NSMutableArray array];
        [days enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            MTTDailyWeatherInformation *weatherInformation = [[MTTDailyWeatherInformation alloc] initWithData:obj];
            [parsedDays addObject:weatherInformation];
        }];
        self.days = [NSArray arrayWithArray:parsedDays];
    }
    else if ([key isEqualToString:@"request"]) {
        NSDictionary *dictionaryData = [(NSArray *)value firstObject];
        self.request = [[MTTRequest alloc] initWithData:dictionaryData];
        
    }
    else {
        if ([self respondsToSelector:@selector(key)]) {
            [super setValue:value forKey:key];
        }
    }
}

@end
