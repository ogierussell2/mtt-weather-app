//
//  MTTAstronomy.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTAstronomy : NSObject

@property (nonatomic, copy) NSString *moonrise;
@property (nonatomic, copy) NSString *moonset;
@property (nonatomic, copy) NSString *sunrise;
@property (nonatomic, copy) NSString *sunset;

@end
