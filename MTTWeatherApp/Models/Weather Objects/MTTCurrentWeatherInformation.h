//
//  MTTWeatherInformation.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTTWeatherInformation.h"

@interface MTTCurrentWeatherInformation : MTTWeatherInformation

#pragma mark - Properties

@property (nonatomic, copy) NSString *observation_time;
@property (nonatomic, copy) NSString *temp_C;
@property (nonatomic, copy) NSString *temp_F;
@property (nonatomic, copy) NSString *winddir16Point;
@property (nonatomic, copy) NSString *winddirDegree;
@property (nonatomic, copy) NSString *windspeedKmph;
@property (nonatomic, copy) NSString *windspeedMiles;

@end
