//
//  MTTDailyWeatherInformation.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import Foundation;
#import "MTTHourlyWeatherInformation.h"
#import "MTTAstronomy.h"

static NSString *kMinimumTemperature = @"minimumTemperature";
static NSString *kMaximumTemperature = @"maximumTemperature";

@interface MTTDailyWeatherInformation : NSObject

#pragma mark - Properties

@property (nonatomic, strong) NSArray *hourly;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, strong) MTTAstronomy *astronomy;

#pragma mark - Initialization

- (instancetype)initWithData:(NSDictionary *)data;

#pragma mark - Helpers

- (NSDictionary *)minimumAndMaximumTemperatures;
- (NSString *)middleOfDayWeatherDescription;
- (NSString *)middleOfDayWeatherIcon;

@end
