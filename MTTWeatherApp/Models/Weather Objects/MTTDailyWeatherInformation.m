//
//  MTTDailyWeatherInformation.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTDailyWeatherInformation.h"
#import "MJExtension.h"

@implementation MTTDailyWeatherInformation

#pragma mark - Initialization

- (instancetype)initWithData:(NSDictionary *)data {
    self = [super init];
    
    if (self) {
        [self setValuesForKeysWithDictionary:data]
        ;
    }
    
    return self;
    
}

- (void)setValue:(id)value forKey:(NSString *)key {
    
    if ([key isEqualToString:NSStringFromSelector(@selector(astronomy))]) {
        NSDictionary *dictionaryData = [value firstObject];
        self.astronomy = [MTTAstronomy objectWithKeyValues:dictionaryData];
    }
    else if ([key isEqualToString:NSStringFromSelector(@selector(hourly))]) {
        NSMutableArray *hourlyArray = [NSMutableArray array];
        [value enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            MTTHourlyWeatherInformation *hourlyInformation =[MTTHourlyWeatherInformation objectWithKeyValues:obj];
            [hourlyArray addObject:hourlyInformation];
        }];
        self.hourly = [NSArray arrayWithArray:hourlyArray];
    }
    else if ([key isEqualToString:NSStringFromSelector(@selector(date))]) {
        self.date = value;
    }
    else {
        if ([self respondsToSelector:@selector(key)]) {
            [super setValue:value forKey:key];
        }
    }
}

#pragma mark - Helpers

- (NSDictionary *)minimumAndMaximumTemperatures {
    
    NSInteger invalidTemp = -9999;
    
    __block NSInteger minimumTemperature = invalidTemp;
    __block NSInteger maximumTemperature = invalidTemp;
    
    [self.hourly enumerateObjectsUsingBlock:^(MTTHourlyWeatherInformation *hourly, NSUInteger idx, BOOL *stop) {
        if (minimumTemperature == invalidTemp && maximumTemperature == invalidTemp) {
            minimumTemperature = hourly.tempC;
            maximumTemperature = hourly.tempC;
        }
        else if (hourly.tempC > maximumTemperature) {
            maximumTemperature = hourly.tempC;
        }
        else if (hourly.tempC < minimumTemperature) {
            minimumTemperature = hourly.tempC;
        }
        
    }];
    
    if (minimumTemperature == invalidTemp) {
        minimumTemperature = 0;
    }
    
    if (maximumTemperature == invalidTemp) {
        maximumTemperature = 0;
    }
    
    return @{kMinimumTemperature: @(minimumTemperature), kMaximumTemperature: @(maximumTemperature)};
}

- (NSString *)middleOfDayWeatherDescription {

    MTTHourlyWeatherInformation *hourly = [self hourlyInfomationForMiddleOfDay];
    
    if (hourly) {
        return hourly.weatherDesc;
    }
    
    return @"";
}

- (NSString *)middleOfDayWeatherIcon {
    
    MTTHourlyWeatherInformation *hourly = [self hourlyInfomationForMiddleOfDay];
    
    if (hourly) {
        return hourly.weatherIconUrl;
    }
    
    return @"";
}

- (MTTHourlyWeatherInformation *)hourlyInfomationForMiddleOfDay {
    
    if (self.hourly.count > 0) {
        MTTHourlyWeatherInformation *hourlyInformation = [self.hourly objectAtIndex:self.hourly.count / 2];
        return hourlyInformation;
    }
    
    return nil;
}

@end
