//
//  MTTDetailedWeatherInformation.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTTWeatherInformation.h"

@interface MTTHourlyWeatherInformation : MTTWeatherInformation

#pragma mark - Properties

@property (nonatomic, assign) NSInteger chanceoffog;
@property (nonatomic, assign) NSInteger chanceoffrost;
@property (nonatomic, assign) NSInteger chanceofhightemp;
@property (nonatomic, assign) NSInteger chanceofovercast;
@property (nonatomic, assign) NSInteger chanceofrain;
@property (nonatomic, assign) NSInteger chanceofremdry;
@property (nonatomic, assign) NSInteger chanceofsnow;
@property (nonatomic, assign) NSInteger chanceofsunshine;
@property (nonatomic, assign) NSInteger chanceofthunder;
@property (nonatomic, assign) NSInteger chanceofwindy;
@property (nonatomic, assign) NSInteger DewPointF;
@property (nonatomic, assign) NSInteger HeatIndexC;
@property (nonatomic, assign) NSInteger HeatIndexF;
@property (nonatomic, assign) NSInteger tempC;
@property (nonatomic, assign) NSInteger tempF;
@property (nonatomic, assign) NSInteger time;

#pragma mark - Helpers

+ (NSArray *)runtimeProperties;
- (NSString *)hourStringForHour;

@end
