//
//  MTTDetailedWeatherInformation.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTHourlyWeatherInformation.h"
#import "MTTRuntimePropertiesForClass.h"

@implementation MTTHourlyWeatherInformation

+ (NSArray *)runtimeProperties {
    NSArray *runtimeProperties = [MTTRuntimePropertiesForClass propertyNamesForClass:self.class withParentClass:self.superclass];
    NSArray *ignorableProperties = [self ignorableProperties];
    
    NSMutableArray *tempProperties = [NSMutableArray arrayWithArray:runtimeProperties];
    [ignorableProperties enumerateObjectsUsingBlock:^(NSString *ignorableProperty, NSUInteger idx, BOOL *stop) {
        if ([tempProperties containsObject:ignorableProperty]) {
            [tempProperties removeObject:ignorableProperty];
        }
    }];
    
    return [NSArray arrayWithArray:tempProperties];
}

+ (NSArray *)ignorableProperties {
    return @[NSStringFromSelector(@selector(weatherDesc)), NSStringFromSelector(@selector(weatherIconUrl)), NSStringFromSelector(@selector(time))];
}

- (NSString *)hourStringForHour {
    
    NSInteger hour = self.time;
    
    if (hour == 0) {
        return @"12:00 AM";
    }
    else {
        if (hour >= 100) {
            hour = hour / 100;
            
            if (hour < 12) {
                return [NSString stringWithFormat:@"%ld:00 AM", (long)hour];
            }
            else {
                return [NSString stringWithFormat:@"%ld:00 PM", (long)hour];
            }
        }
        
    }
    
    return @"";
}

@end
