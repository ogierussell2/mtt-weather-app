//
//  MTTRequest.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTRequest : NSObject

#pragma mark - Properties

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *country;

#pragma mark - Initializers

- (instancetype)initWithData:(NSDictionary *)data;

@end
