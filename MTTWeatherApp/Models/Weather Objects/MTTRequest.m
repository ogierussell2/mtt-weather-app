//
//  MTTRequest.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTRequest.h"
#import "NSString+MTT.h"

@implementation MTTRequest

- (instancetype)initWithData:(NSDictionary *)data {
    self = [super init];
    
    if (self) {
        NSString *query = @"query";
        if ([data objectForKey:query]) {
            NSString *locationInfo = [data objectForKey:query];
            NSArray *locationComponents = [locationInfo stringSplitByCommas];
            self.city = [locationComponents firstObject];
            if (locationComponents.count > 1) {
                self.country = [locationComponents objectAtIndex:1];
            }
        }
        
        if ([data objectForKey:NSStringFromSelector(@selector(type))]) {
            self.type = [data objectForKey:NSStringFromSelector(@selector(type))];
        }
    }
    
    return self;
}

@end
