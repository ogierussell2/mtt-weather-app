//
//  MTTWeatherInformation.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import Foundation;

@interface MTTWeatherInformation : NSObject

@property (nonatomic, assign) NSInteger visibility;
@property (nonatomic, copy) NSString *weatherDesc;
@property (nonatomic, copy) NSString *weatherIconUrl;
@property (nonatomic, assign) NSInteger weatherCode;
@property (nonatomic, assign) NSInteger cloudcover;
@property (nonatomic, assign) NSInteger FeelsLikeC;
@property (nonatomic, assign) NSInteger FeelsLikeF;
@property (nonatomic, assign) NSInteger humidity;
@property (nonatomic, assign) NSInteger precipMM;
@property (nonatomic, assign) NSInteger pressure;

@end
