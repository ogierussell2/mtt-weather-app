//
//  MTTWeatherInformation.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTWeatherInformation.h"

@implementation MTTWeatherInformation

#pragma mark - Setup

- (void)setValue:(id)value forKey:(NSString *)key {
    
    if ([key isEqualToString:NSStringFromSelector(@selector(weatherDesc))]) {
        NSDictionary *data = [(NSArray *)value firstObject];
        self.weatherDesc = [data objectForKey:NSStringFromSelector(@selector(value))];
    }
    else if ([key isEqualToString:NSStringFromSelector(@selector(weatherIconUrl))]) {
        NSDictionary *data = [(NSArray *)value firstObject];
        self.weatherIconUrl = [data objectForKey:NSStringFromSelector(@selector(value))];
    }
    else {
        [super setValue:value forKey:key];
    }
}

@end
