//
//  MTTAPIOperationManager.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface MTTAPIOperationManager : AFHTTPRequestOperationManager

- (void)getWeatherForCity:(NSString *)city withCompletionBlock:(void (^)(id weatherData))completionBlock withFailureBlock:(void (^)(NSError *error))failureBlock;

@end
