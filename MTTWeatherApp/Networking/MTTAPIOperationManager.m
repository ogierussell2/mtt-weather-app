//
//  MTTAPIOperationManager.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTAPIOperationManager.h"

// models
#import "MTTWeatherRequest.h"
#import "MTTWeatherResponse.h"

@implementation MTTAPIOperationManager

- (instancetype)init {
    self = [super initWithBaseURL:[NSURL URLWithString:@"http://api.worldweatheronline.com/"]];
    
    if (self) {
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

- (void)getWeatherForCity:(NSString *)city withCompletionBlock:(void (^)(id weatherData))completionBlock withFailureBlock:(void (^)(NSError *error))failureBlock {
    
    if (!city) {
        if (failureBlock) {
            failureBlock(nil);
            return;
        }
    }
    
    MTTWeatherRequest *weatherRequest = [[MTTWeatherRequest alloc] initWithCity:city];
    
    [self GET:@"free/v2/weather.ashx" parameters:[weatherRequest dictionary] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        MTTWeatherResponse *weatherResponse = [[MTTWeatherResponse alloc] initWithData:responseObject];
        
        if (completionBlock) {
            completionBlock(weatherResponse);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

@end
