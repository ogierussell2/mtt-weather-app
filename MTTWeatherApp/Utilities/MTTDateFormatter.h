//
//  MTTDateFormatter.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 05/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTDateFormatter : NSObject

+ (NSString *)dayWithDateForString:(NSString *)stringDate;

@end
