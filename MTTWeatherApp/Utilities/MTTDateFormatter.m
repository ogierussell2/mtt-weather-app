//
//  MTTDateFormatter.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 05/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTDateFormatter.h"

static NSDateFormatter *dayWithDateFormatter;
static NSDateFormatter *yearMonthDayFormatter;

@implementation MTTDateFormatter

+ (void)initialize {
    [super initialize];
    
    yearMonthDayFormatter = [[NSDateFormatter alloc] init];
    yearMonthDayFormatter.dateFormat = @"YYYY-MM-dd";
    
    dayWithDateFormatter = [[NSDateFormatter alloc] init];
    dayWithDateFormatter.dateFormat = @"EEE dd MMM";
}

+ (NSString *)dayWithDateForString:(NSString *)stringDate {
    NSDate *date = [self yearMonthDayFromString:stringDate];
    return [dayWithDateFormatter stringFromDate:date];
}

+ (NSDate *)yearMonthDayFromString:(NSString *)stringDate {
    return [yearMonthDayFormatter dateFromString:stringDate];
}

@end
