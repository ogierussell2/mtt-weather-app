//
//  MTTFadeTransitionManager.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 05/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;

@interface MTTFadeTransitionManager : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) BOOL presenting;

@end
