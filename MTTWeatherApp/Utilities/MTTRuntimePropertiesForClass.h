//
//  MTTRuntimePropertiesForClass.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTRuntimePropertiesForClass : NSObject

+ (NSArray *)propertyNamesForClass:(Class)className withParentClass:(Class)parentClass;

@end
