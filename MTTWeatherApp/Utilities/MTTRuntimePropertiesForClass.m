//
//  MTTRuntimePropertiesForClass.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTRuntimePropertiesForClass.h"
#import <objc/runtime.h>

@implementation MTTRuntimePropertiesForClass

+ (NSArray *)propertyNamesForClass:(Class)className withParentClass:(Class)parentClass {
    
    // want to figure out a way to access parent without passing it in, wanted to get Class superclass until not nsobject but couldn't find a way to access something similar to Class.parentClass 
    NSMutableArray *propertyNames = [NSMutableArray array];
    unsigned int count, subclassCount;
    objc_property_t *properties = class_copyPropertyList(className, &count);
    for (subclassCount = 0; subclassCount < count; subclassCount++) {
        objc_property_t property = properties[subclassCount];
        NSString *propertyName = [NSString stringWithCString:property_getName(property) encoding:NSASCIIStringEncoding];
        [propertyNames addObject:propertyName];
    }
    
    unsigned int newcount, parentCount;
    objc_property_t *parentProperties = class_copyPropertyList(parentClass, &newcount);
    for (parentCount = 0; parentCount < newcount; parentCount++) {
        objc_property_t property = parentProperties[parentCount];
        NSString *propertyName = [NSString stringWithCString:property_getName(property) encoding:NSASCIIStringEncoding];
        [propertyNames addObject:propertyName];
    }

    
    return propertyNames;
}

@end
