//
//  MTTUserDefaults.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTUserDefaults : NSObject

#pragma mark - Cities

+ (void)addCity:(NSString *)city;
+ (void)removeCity:(NSString *)city;
+ (BOOL)cityAlreadyExists:(NSString *)city;
+ (NSArray *)cities;

@end
