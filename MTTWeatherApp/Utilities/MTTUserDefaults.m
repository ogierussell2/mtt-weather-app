//
//  MTTUserDefaults.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#define DEFAULTS [NSUserDefaults standardUserDefaults]

#import "MTTUserDefaults.h"

// models
#import "MTTCityData.h"

static NSString *kCities = @"cities";

@implementation MTTUserDefaults

#pragma mark - Cities

+ (void)addCity:(NSString *)city {
   
    NSMutableArray *savedCities = [NSMutableArray arrayWithArray:[DEFAULTS objectForKey:kCities]];
    
    if (!savedCities) {
        savedCities = [NSMutableArray array];
    }
    
    if (savedCities.count == 0) {
        [savedCities addObject:city];
    }
    else {
        [savedCities insertObject:city atIndex:0];
    }
    
    [DEFAULTS setObject:[NSArray arrayWithArray:savedCities] forKey:kCities];
}

+ (void)removeCity:(NSString *)city {
    
    NSMutableArray *savedCities = [NSMutableArray arrayWithArray:[DEFAULTS objectForKey:kCities]];

    if ([savedCities containsObject:city]) {
        [savedCities removeObject:city];
    }
    
    [DEFAULTS setObject:[NSArray arrayWithArray:savedCities] forKey:kCities];
}

+ (BOOL)cityAlreadyExists:(NSString *)city {
    
    NSMutableArray *savedCities = [NSMutableArray arrayWithArray:[DEFAULTS objectForKey:kCities]];
    return [savedCities containsObject:city];
}

+ (NSArray *)cities {
   
    NSArray *cityNames = [DEFAULTS objectForKey:kCities];
    NSMutableArray *cityDataObjects = [NSMutableArray array];
    
    [cityNames enumerateObjectsUsingBlock:^(NSString *city, NSUInteger idx, BOOL *stop) {
        MTTCityData *cityData = [[MTTCityData alloc] init];
        cityData.city = city;
        [cityDataObjects addObject:cityData];
    }];
    
    return [NSArray arrayWithArray:cityDataObjects];
}

@end
