//
//  MTTAddCityViewController.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTViewController.h"

#import "MTTAddCityDelegate.h"

@interface MTTAddCityViewController : MTTViewController

#pragma mark - Setup

- (void)setDelegate:(id<MTTAddCityDelegate>)delegate;

@end
