//
//  MTTAddCityViewController.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTAddCityViewController.h"

// api
#import "MTTAPIOperationManager.h"

// view controllers
#import "MTTCitiesViewController.h"

// models
#import "MTTCityData.h"
#import "MTTWeatherResponse.h"

// utilities
#import "MTTUserDefaults.h"

@interface MTTAddCityViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *cityTextField;
@property (nonatomic, assign) id<MTTAddCityDelegate> addCityDelegate;

@end

@implementation MTTAddCityViewController

#pragma mark - Start of Life

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.cityTextField becomeFirstResponder];
    self.cityTextField.delegate = self;
}

#pragma mark - Setup

- (void)setDelegate:(id<MTTAddCityDelegate>)delegate {
    self.addCityDelegate = delegate;
}

#pragma mark - IBActions

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - IBActions

- (IBAction)closeModal:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (IBAction)search:(id)sender {
    
    if ([[self.cityTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]) {
        [self showErrorAlert];
        return;
    }
    
    MTTAPIOperationManager *manager = [[MTTAPIOperationManager alloc] init];
    [manager getWeatherForCity:self.cityTextField.text withCompletionBlock:^(MTTWeatherResponse *weatherResponse) {
        
        if (!weatherResponse.request.city || [weatherResponse.request.city isEqualToString:@""]) {
            [self showErrorAlert];
            return;
        }
        
        if ([MTTUserDefaults cityAlreadyExists:weatherResponse.request.city]) {
            [self showDuplicationAlertForCityName:weatherResponse.request.city];
            return;
        }
        
        MTTCityData *cityData = [[MTTCityData alloc] init];
        cityData.weatherData = weatherResponse;
        cityData.city = weatherResponse.request.city;
        [self.addCityDelegate addCityData:cityData];
        [self dismissViewControllerAnimated:YES completion:^{}];
    } withFailureBlock:^(NSError *error) {
        [self showErrorAlert];
    }];
}

#pragma mark - Alerts

- (void)showErrorAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No weather data for this city. Please try a different city." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertView show];
    });
}

- (void)showDuplicationAlertForCityName:(NSString *)cityName {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:[NSString stringWithFormat:@"Looks like you have already added %@ to the list", cityName] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertView show];
    });
}

#pragma mark - TextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self search:self.cityTextField];
    return YES;
}

@end
