//
//  ViewController.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTCitiesViewController.h"
#import "MTTAPIOperationManager.h"

// custom ui
#import "MTTCityTableViewCell.h"
#import "MTTSadPathTableViewCell.h"

// models
#import "MTTWeatherResponse.h"
#import "MTTCityData.h"

// view controllers
#import "MTTAddCityViewController.h"
#import "MTTWeatherForecastViewController.h"

// tableview
#import "MTTCitiesTableViewDatasource.h"
#import "MTTCitiesTableViewDelegate.h"

// delegates
#import "MTTAddCityDelegate.h"
#import "MTTSelectedRowDelegate.h"
#import "MTTDeleteRowDelegate.h"

// utilities
#import "MTTUserDefaults.h"

// categories
#import "UIColor+MTT.h"
#import "UIViewController+CustomAnimation.h"

@interface MTTCitiesViewController () <MTTSelectedRowDelegate, MTTDeleteRowDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet MTTCitiesTableViewDatasource *datasource;
@property (nonatomic, weak) IBOutlet MTTCitiesTableViewDelegate *delegate;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation MTTCitiesViewController

#pragma mark - Start of Life

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerCells];
    [self setupDatasource];
    [self setupDelegate];
    [self setupRefreshControl];
    [self.tableView reloadData];
    [self.datasource fetchDataWithCompletionBlock:^{}];
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
}

- (void)setupDatasource {
    [self.datasource setData:[MTTUserDefaults cities] forTableView:self.tableView];
    [self.datasource setDeleteRowDelegate:self];
}

- (void)setupDelegate {
    [self.delegate setSelectedRowDelegate:self];
    self.delegate.heightForCellAtIndexPath = ^CGFloat(NSIndexPath *indexPath) {
        id object = [self.datasource objectForIndexPath:indexPath];
        if ([object isKindOfClass:[MTTCityData class]]) {
            return [MTTCityTableViewCell height];
        }
        else {
            return self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
        }
    };
}

- (void)setupRefreshControl {
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.refreshControl.superview sendSubviewToBack:self.refreshControl];
}

#pragma mark - Helpers

- (void)registerCells {
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass(MTTCityTableViewCell.class) bundle:nil] forCellReuseIdentifier:NSStringFromClass(MTTCityTableViewCell.class)];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass(MTTSadPathTableViewCell.class) bundle:nil] forCellReuseIdentifier:NSStringFromClass(MTTSadPathTableViewCell.class)];
}

#pragma mark - IBActions

- (IBAction)addCity:(id)sender {
    
    if ([self.datasource numberOfItems] == 5) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Due to api limitations you can only add a maximum of 5 cities. Please delete one of your existing cities to add a new one." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [alertView show];
        });
    }
    else {
        [self showAddCity];
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:[MTTWeatherForecastViewController segueIdentifier]]) {
        MTTWeatherForecastViewController *controller = segue.destinationViewController;
        [controller setData:sender];
    }
    else {
        [super prepareForSegue:segue sender:sender];
    }
}

#pragma mark - Add City Delegate

- (void)addCityData:(MTTCityData *)cityData {
    [self.datasource addDataToStartOfArray:cityData
     ];
    [self.tableView reloadData];
}

#pragma mark - Delete City Delegate

- (void)tableView:(UITableView *)tableView didDeleteCityData:(MTTCityData *)cityData {
    
    if (!cityData.city) {
        return;
    }
    
    [MTTUserDefaults removeCity:cityData.city];
}

#pragma mark - Delegates

- (void)scrollView:(UIScrollView *)scrollView didSelectCellAtIndexPath:(NSIndexPath *)indexPath {
    id object = [self.datasource objectForIndexPath:indexPath];
    
    if ([object isKindOfClass:[MTTCityData class]]) {
        
        MTTCityData *cityData = object;
        
        if (!cityData.weatherData) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"We are unable to receive weather data at this time, please check your internet connection and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [alert show];
            });
        }
        else {
            [self performSegueWithIdentifier:[MTTWeatherForecastViewController segueIdentifier] sender:object];
        }
    }
}

#pragma mark - UIRefreshControl 

- (void)refresh:(UIRefreshControl *)refreshControl {
    
    [self.datasource fetchDataWithCompletionBlock:^{
        [refreshControl endRefreshing];
    }];
}

@end
