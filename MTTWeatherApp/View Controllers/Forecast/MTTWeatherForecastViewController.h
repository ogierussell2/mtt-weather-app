//
//  MTTWeatherForecastViewController.h
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTViewController.h"

// models
#import "MTTCityData.h"

@interface MTTWeatherForecastViewController : MTTViewController

- (void)setData:(MTTCityData *)cityData;

@end
