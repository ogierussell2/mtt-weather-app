//
//  MTTWeatherForecastViewController.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 02/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTWeatherForecastViewController.h"

// tableView
#import "MTTForecastTableViewDatasource.h"
#import "MTTForecastTableViewDelegate.h"

// cells
#import "MTTCurrentDayForecastTableViewCell.h"
#import "MTTForecastTableViewCell.h"
#import "MTTDetailedDataTableViewCell.h"

// delegate
#import "MTTSelectedRowDelegate.h"

@interface MTTWeatherForecastViewController () <MTTSelectedRowDelegate>

@property (nonatomic, strong) MTTCityData *cityData;
@property (nonatomic, weak) IBOutlet MTTForecastTableViewDatasource *datasource;
@property (strong, nonatomic) IBOutlet MTTForecastTableViewDelegate *delegate;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation MTTWeatherForecastViewController

#pragma mark - Start of Life

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerCells];
    [self.delegate setSelectedRowDelegate:self];
    [self.datasource setData:self.cityData.weatherData.days withCity:self.cityData.city];
    [self.tableView reloadData];
}

#pragma mark - Setup

- (void)setData:(MTTCityData *)cityData {
    self.cityData = cityData;
}

#pragma mark - Helpers

- (void)registerCells {
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass(MTTCurrentDayForecastTableViewCell.class) bundle:nil] forCellReuseIdentifier:NSStringFromClass(MTTCurrentDayForecastTableViewCell.class)];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass(MTTForecastTableViewCell.class) bundle:nil] forCellReuseIdentifier:NSStringFromClass(MTTForecastTableViewCell.class)];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass(MTTDetailedDataTableViewCell.class) bundle:nil] forCellReuseIdentifier:NSStringFromClass(MTTDetailedDataTableViewCell.class)];
}

#pragma mark - Selection Delegate

- (void)scrollView:(UIScrollView *)scrollView didSelectCellAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.datasource isPlaceholderObjectAtIndex:indexPath.row]) {
        return;
    }
    
    BOOL success = [self.datasource successfullyInsertPlaceholderForIndex:indexPath.row];
    
    if (success) {
        NSIndexPath *newIndexPath =[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView scrollToRowAtIndexPath:newIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];

    }
}

- (void)scrollView:(UIScrollView *)scrollView didDeselectCellAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.datasource isPlaceholderObjectAtIndex:indexPath.row]) {
        return;
    }
    
    NSInteger index = indexPath.row + 1;
    BOOL success = [self.datasource successfullyRemovedPlaceHolderForIndex:index];
    
    if (success) {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

@end
