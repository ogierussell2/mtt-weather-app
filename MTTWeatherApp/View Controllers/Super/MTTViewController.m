//
//  MTTViewController.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 01/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

#import "MTTViewController.h"

@interface MTTViewController ()

@end

@implementation MTTViewController

+ (NSString *)segueIdentifier {
    return NSStringFromClass(self.class);
}

@end
