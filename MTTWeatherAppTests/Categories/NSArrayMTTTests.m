//
//  NSArrayMTTTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "NSArray+MTT.h"

@interface NSArrayMTTTests : XCTestCase

@end

@implementation NSArrayMTTTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark - Safe Object

- (void)test_safeObjectAtIndex_ValidIndex {
    NSArray *array = @[@"test", @"test", @"test"];
    id object = [array safeObjectAtIndex:1];
    
    XCTAssertNotNil(object);
}

- (void)test_safeObjectAtIndex_LargerIndex {
    NSArray *array = @[@"test", @"test", @"test"];
    id object = [array safeObjectAtIndex:5];
    
    XCTAssertNil(object);
    
}

- (void)test_safeObjectAtIndex_LowerIndex {
    NSArray *array = @[@"test", @"test", @"test"];
    id object = [array safeObjectAtIndex:-1];
    
    XCTAssertNil(object);
}

#pragma mark - Safe Removal

- (void)test_canSafelyRemoveObjectAtIndex_ValidIndex {
    NSArray *array = @[@"test", @"test", @"test"];
    BOOL canRemove = [array canSafelyRemoveObjectAtIndex:1];
    
    XCTAssertTrue(canRemove);
}

- (void)test_canSafelyRemoveObjectAtIndex_LargerIndex {
    NSArray *array = @[@"test", @"test", @"test"];
    BOOL canRemove = [array canSafelyRemoveObjectAtIndex:5];
    
    XCTAssertFalse(canRemove);
}

- (void)test_canSafelyRemoveObjectAtIndex_LowerIndex {
    NSArray *array = @[@"test", @"test", @"test"];
    BOOL canRemove = [array canSafelyRemoveObjectAtIndex:-1
                      ];
    
    XCTAssertFalse(canRemove);
}

@end
