//
//  NSStringMTTTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "NSString+MTT.h"

@interface NSStringMTTTests : XCTestCase

@end

@implementation NSStringMTTTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_stringSplitByCommas_CorrectNumberOfResults {
    NSString *testString = @"this, is , a ,comman, seperated, string";
    NSArray *components = [testString stringSplitByCommas];
    
    XCTAssertTrue(components.count == 6);
}

- (void)test_stringSplitByCommas_NoCommas {
    
    NSString *testString = @"string with no commas";
    NSArray *components = [testString stringSplitByCommas];
    
    XCTAssertTrue(components.count == 1);
    
}

- (void)test_stringSplitByCommas_NoWhiteSpaces {
    
    NSString *testString = @"string,With,No,Whitespaces";
    NSArray *components = [testString stringSplitByCommas];
    
    XCTAssertTrue(components.count == 4);
}

- (void)test_stringSplitByCommas_RemovesWhitespaces {
    
    NSString *testString = @"this , has ,     lots, of  spaces";
    NSArray *components = [testString stringSplitByCommas];
    
    __block BOOL hasWhitespaces = NO;
    
    [components enumerateObjectsUsingBlock:^(NSString *component, NSUInteger idx, BOOL *stop) {
        NSRange whiteSpaceRange = [component rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
        if (whiteSpaceRange.location != NSNotFound) {
            hasWhitespaces = YES;
        }
    }];
    
    XCTAssertFalse(hasWhitespaces);
}

@end
