//
//  MTTDetailedDataTableViewCellTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "OCMock.h"
#import "MTTDetailedDataTableViewCell.h"

@interface MTTDetailedDataTableViewCell ()

@property (nonatomic, strong) NSArray *hourlyData;

- (void)setupDatasourceWithProperties:(NSArray *)properties withHourlyInformation:(NSArray *)hourlyInformation;
- (void)setupDelegate;
- (void)registerCells;

@end

@interface MTTDetailedDataTableViewCellTests : XCTestCase

@end

@implementation MTTDetailedDataTableViewCellTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)tests_setData_DatasourceSetupCalled {
    
    MTTDetailedDataTableViewCell *detailedCell = [[MTTDetailedDataTableViewCell alloc] init];
    id mockCell = [OCMockObject partialMockForObject:detailedCell];
    
    [[[mockCell expect] andForwardToRealObject] setupDatasourceWithProperties:[OCMArg any] withHourlyInformation:[OCMArg any]];
    [detailedCell setData:MTTDailyWeatherInformation.new];
    
    [mockCell verify];
}

- (void)tests_setData_DelegateSetupCalled {
    
    MTTDetailedDataTableViewCell *detailedCell = [[MTTDetailedDataTableViewCell alloc] init];
    id mockCell = [OCMockObject partialMockForObject:detailedCell];
    
    [[[mockCell expect] andForwardToRealObject] setupDelegate];
    [detailedCell setData:MTTDailyWeatherInformation.new];
    
    [mockCell verify];
}

- (void)tests_setData_RegisterCellsCalled {
    
    MTTDetailedDataTableViewCell *detailedCell = [[MTTDetailedDataTableViewCell alloc] init];
    id mockCell = [OCMockObject partialMockForObject:detailedCell];
    
    [[[mockCell expect] andForwardToRealObject] registerCells];
    [detailedCell setData:MTTDailyWeatherInformation.new];
    
    [mockCell verify];
}

- (void)test_setData_HourlyDataSet {
    
    MTTDetailedDataTableViewCell *detailedCell = [[MTTDetailedDataTableViewCell alloc] init];
    MTTDailyWeatherInformation *weather = [[MTTDailyWeatherInformation alloc] init];
    NSArray *hourly = @[[[MTTHourlyWeatherInformation alloc] init]];
    weather.hourly = hourly;
    
    [detailedCell setData:weather];
    
    XCTAssertTrue([detailedCell.hourlyData isEqual:hourly]);
}

@end
