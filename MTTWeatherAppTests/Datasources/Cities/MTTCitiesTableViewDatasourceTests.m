//
//  MTTCitiesTableViewDatasourceTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "MTTCitiesTableViewDatasource.h"

@interface MTTCitiesTableViewDatasource (Testing)

@property (nonatomic, strong) NSArray *data;

- (NSInteger)indexForObjectMatchingCity:(NSString *)city;

@end

@interface MTTCitiesTableViewDatasourceTests : XCTestCase

@end

@implementation MTTCitiesTableViewDatasourceTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_indexForObejctMatchingCity_ValidCityIndex {
    
    MTTCitiesTableViewDatasource *datasource = [[MTTCitiesTableViewDatasource alloc] init];
    MTTCityData *cityData = MTTCityData.new;
    cityData.city = @"Dublin";
    MTTCityData *limerickCityData = MTTCityData.new;
    limerickCityData.city = @"Limerick";
    
    NSArray *cities = @[cityData, limerickCityData];
    [datasource setData:cities forTableView:UITableView.new];
    NSInteger index = [datasource indexForObjectMatchingCity:@"Limerick"];
    XCTAssertTrue(index == 1);
    
}

- (void)test_indexForObejctMatchingCity_InvalidCityIndex {
    
    MTTCitiesTableViewDatasource *datasource = [[MTTCitiesTableViewDatasource alloc] init];
    MTTCityData *cityData = MTTCityData.new;
    cityData.city = @"Glasgow";
    MTTCityData *limerickCityData = MTTCityData.new;
    limerickCityData.city = @"Limerick";
    
    NSArray *cities = @[cityData, limerickCityData];
    [datasource setData:cities forTableView:UITableView.new];
    NSInteger index = [datasource indexForObjectMatchingCity:@"Dublin"];
    XCTAssertTrue(index == -1);
}

- (void)test_addDataToStartOfArray_ValidData {
    
    MTTCitiesTableViewDatasource *datasource = [[MTTCitiesTableViewDatasource alloc] init];
    MTTCityData *cityData = MTTCityData.new;
    cityData.city = @"Glasgow";
    NSArray *cities = @[cityData];
    [datasource setData:cities forTableView:UITableView.new];

    MTTCityData *limerickCityData = MTTCityData.new;
    limerickCityData.city = @"Limerick";
    [datasource addDataToStartOfArray:limerickCityData];
    
    XCTAssertTrue([[datasource.data firstObject] isEqual:limerickCityData]);

}

- (void)test_addDataToStartOfArray_InvalidData {
    
    MTTCitiesTableViewDatasource *datasource = [[MTTCitiesTableViewDatasource alloc] init];
    MTTCityData *cityData = MTTCityData.new;
    cityData.city = @"Glasgow";
    NSArray *cities = @[cityData];
    [datasource setData:cities forTableView:UITableView.new];
    [datasource addDataToStartOfArray:nil];
    
    XCTAssertTrue([[datasource.data firstObject] isEqual:cityData]);
}

- (void)test_objectForIndexPath_ValidIndexPath {
    
    MTTCitiesTableViewDatasource *datasource = [[MTTCitiesTableViewDatasource alloc] init];
    MTTCityData *cityData = MTTCityData.new;
    cityData.city = @"Glasgow";
    MTTCityData *limerickCityData = MTTCityData.new;
    limerickCityData.city = @"Limerick";
    
    NSArray *cities = @[cityData, limerickCityData];
    [datasource setData:cities forTableView:UITableView.new];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    XCTAssertTrue([[datasource objectForIndexPath:indexPath] isEqual:limerickCityData]);
}

- (void)test_objectForIndexPath_InvalidIndexPath {
    
    MTTCitiesTableViewDatasource *datasource = [[MTTCitiesTableViewDatasource alloc] init];
    MTTCityData *cityData = MTTCityData.new;
    cityData.city = @"Glasgow";
    MTTCityData *limerickCityData = MTTCityData.new;
    limerickCityData.city = @"Limerick";
    
    NSArray *cities = @[cityData, limerickCityData];
    [datasource setData:cities forTableView:UITableView.new];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:5 inSection:0];
    XCTAssertNil([datasource objectForIndexPath:indexPath]);
    
}

@end
