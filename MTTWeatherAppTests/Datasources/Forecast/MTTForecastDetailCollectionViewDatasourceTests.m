//
//  MTTForecastDetailCollectionViewDatasourceTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "MTTForecastDetailCollectionViewDatasource.h"

@interface MTTForecastDetailCollectionViewDatasource (Testing)

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *properties;
@end

@interface MTTForecastDetailCollectionViewDatasourceTests : XCTestCase

@end

@implementation MTTForecastDetailCollectionViewDatasourceTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_setData_PropertiesSet {
    
    MTTForecastDetailCollectionViewDatasource *datasource = [[MTTForecastDetailCollectionViewDatasource alloc] init];
    NSArray *data = @[@"test data"];
    NSArray *properties = @[@"propertyName"];
    [datasource setData:data forProperties:properties];
        
    XCTAssertTrue([datasource.data isEqual:data]);
    XCTAssertTrue([datasource.properties isEqual:properties]);
}

@end
