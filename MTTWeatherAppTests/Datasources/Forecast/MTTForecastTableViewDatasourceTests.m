//
//  MTTForecastTableViewDatasourceTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "MTTForecastTableViewDatasource.h"

// models
#import "MTTDailyWeatherInformation.h"
#import "MTTPlaceHolderWeatherObject.h"

@interface MTTForecastTableViewDatasource (Testing)

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSString *city;

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface MTTForecastTableViewDatasourceTests : XCTestCase

@end

@implementation MTTForecastTableViewDatasourceTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_setDataWithCity_PropertiesSet {
    
    MTTForecastTableViewDatasource *datasource = [[MTTForecastTableViewDatasource alloc] init];
    MTTDailyWeatherInformation *dailyWeather = [[MTTDailyWeatherInformation alloc] init];
    dailyWeather.date = @"12-07-2015";
    NSArray *data = @[dailyWeather];
    [datasource setData:data withCity:@"city"];
    
    XCTAssertTrue([datasource.city isEqualToString:@"city"]);
    XCTAssertTrue([datasource.data isEqual:data]);
}

- (void)test_successfullyInsertPlaceholder_ValidIndex {
    
    MTTForecastTableViewDatasource *datasource = [[MTTForecastTableViewDatasource alloc] init];
    MTTDailyWeatherInformation *dailyWeather = [[MTTDailyWeatherInformation alloc] init];
    dailyWeather.date = @"12-07-2015";
    [datasource setData:@[dailyWeather] withCity:@"city"];
    
    XCTAssertTrue([datasource successfullyInsertPlaceholderForIndex:0]);
}

- (void)test_successfullyInsertPlaceholder_LargerIndex {
    
    MTTForecastTableViewDatasource *datasource = [[MTTForecastTableViewDatasource alloc] init];
    MTTDailyWeatherInformation *dailyWeather = [[MTTDailyWeatherInformation alloc] init];
    dailyWeather.date = @"12-07-2015";
    [datasource setData:@[dailyWeather] withCity:@"city"];
    
    XCTAssertFalse([datasource successfullyInsertPlaceholderForIndex:1]);
}

- (void)test_successfullyInsertPlaceholder_LowerIndex {

    MTTForecastTableViewDatasource *datasource = [[MTTForecastTableViewDatasource alloc] init];
    MTTDailyWeatherInformation *dailyWeather = [[MTTDailyWeatherInformation alloc] init];
    dailyWeather.date = @"12-07-2015";
    [datasource setData:@[dailyWeather] withCity:@"city"];
    
    XCTAssertFalse([datasource successfullyInsertPlaceholderForIndex:-1]);
}

- (void)test_successfullyRemovePlaceholder_ValidIndex {
   
    MTTForecastTableViewDatasource *datasource = [[MTTForecastTableViewDatasource alloc] init];
    MTTDailyWeatherInformation *dailyWeather = [[MTTDailyWeatherInformation alloc] init];
    dailyWeather.date = @"12-07-2015";
    MTTPlaceHolderWeatherObject *placeholder = [[MTTPlaceHolderWeatherObject alloc] init];
    [datasource setData:@[dailyWeather, placeholder] withCity:@"city"];
    
    XCTAssertTrue([datasource successfullyRemovedPlaceHolderForIndex:1]);
}

- (void)test_successfullyRemovePlaceholder_LargerIndex {

    MTTForecastTableViewDatasource *datasource = [[MTTForecastTableViewDatasource alloc] init];
    MTTDailyWeatherInformation *dailyWeather = [[MTTDailyWeatherInformation alloc] init];
    dailyWeather.date = @"12-07-2015";
    MTTPlaceHolderWeatherObject *placeholder = [[MTTPlaceHolderWeatherObject alloc] init];
    [datasource setData:@[dailyWeather, placeholder] withCity:@"city"];
    
    XCTAssertFalse([datasource successfullyRemovedPlaceHolderForIndex:3]);
}

- (void)test_successfullyRemovePlaceholder_LowerIndex {
    
    MTTForecastTableViewDatasource *datasource = [[MTTForecastTableViewDatasource alloc] init];
    MTTDailyWeatherInformation *dailyWeather = [[MTTDailyWeatherInformation alloc] init];
    dailyWeather.date = @"12-07-2015";
    MTTPlaceHolderWeatherObject *placeholder = [[MTTPlaceHolderWeatherObject alloc] init];
    [datasource setData:@[dailyWeather, placeholder] withCity:@"city"];
    
    XCTAssertFalse([datasource successfullyRemovedPlaceHolderForIndex:-1]);
}

- (void)test_successfullyRemovePlaceholder_NonPlaceholderIndex {
    MTTForecastTableViewDatasource *datasource = [[MTTForecastTableViewDatasource alloc] init];
    MTTDailyWeatherInformation *dailyWeather = [[MTTDailyWeatherInformation alloc] init];
    dailyWeather.date = @"12-07-2015";
    MTTPlaceHolderWeatherObject *placeholder = [[MTTPlaceHolderWeatherObject alloc] init];
    [datasource setData:@[dailyWeather, placeholder] withCity:@"city"];
    
    XCTAssertFalse([datasource successfullyRemovedPlaceHolderForIndex:0]);
}

@end
