//
//  MTTWeatherRequestTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "MTTWeatherRequest.h"

@interface MTTWeatherRequestTests : XCTestCase

@end

@implementation MTTWeatherRequestTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_initWithCity_PropertiesInitialised {
    
    MTTWeatherRequest *weatherRequest = [[MTTWeatherRequest alloc] initWithCity:@"london"];
    
    XCTAssertTrue([weatherRequest.city isEqualToString:@"london"]);
    XCTAssertTrue([weatherRequest.format isEqualToString:@"json"]);
    XCTAssertTrue([weatherRequest.numberOfDays isEqualToString:@"5"]);
    XCTAssertTrue(weatherRequest.apiKey);
}

- (void)test_dictionary_CorrectValues {
    MTTWeatherRequest *weatherRequest = [[MTTWeatherRequest alloc] initWithCity:@"london"];
    NSDictionary *requestDictionary = [weatherRequest dictionary];
    
    XCTAssertTrue([[requestDictionary objectForKey:@"q"] isEqualToString:@"london"]);
    XCTAssertTrue([[requestDictionary objectForKey:NSStringFromSelector(@selector(format))] isEqualToString:@"json"]);
    XCTAssertTrue([[requestDictionary objectForKey:@"num_of_days"] isEqualToString:@"5"]);
    XCTAssertTrue([requestDictionary objectForKey:@"key"]);
}

- (void)test_dictionary_NilCity {
    
    MTTWeatherRequest *weatherRequest = [[MTTWeatherRequest alloc] initWithCity:nil];
    NSDictionary *requestDictionary = [weatherRequest dictionary];
    
    XCTAssertTrue([[requestDictionary objectForKey:@"q"] isEqualToString:@"Unknown"]);
    XCTAssertTrue([[requestDictionary objectForKey:NSStringFromSelector(@selector(format))] isEqualToString:@"json"]);
    XCTAssertTrue([[requestDictionary objectForKey:@"num_of_days"] isEqualToString:@"5"]);
    XCTAssertTrue([requestDictionary objectForKey:@"key"]);
}

@end
