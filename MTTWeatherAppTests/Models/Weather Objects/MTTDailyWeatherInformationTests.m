//
//  MTTDailyWeatherInformationTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "MTTDailyWeatherInformation.h"

@interface MTTDailyWeatherInformationTests : XCTestCase

@end

@implementation MTTDailyWeatherInformationTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_minimumAndMaximumTemperatures_CorrectValuesReturned {
    
    MTTDailyWeatherInformation *daily = [[MTTDailyWeatherInformation alloc] init];
    MTTHourlyWeatherInformation *hourly = [[MTTHourlyWeatherInformation alloc] init];
    hourly.tempC = 5;
    MTTHourlyWeatherInformation *hourly1 = [[MTTHourlyWeatherInformation alloc] init];
    hourly1.tempC = 50;
    MTTHourlyWeatherInformation *hourly2 = [[MTTHourlyWeatherInformation alloc] init];
    hourly2.tempC = 35;
    NSArray *hourlies = @[hourly, hourly1, hourly2];
    daily.hourly = hourlies;
    
    NSDictionary *temps = [daily minimumAndMaximumTemperatures];
    
    XCTAssertTrue([[temps objectForKey:kMinimumTemperature] integerValue] == 5);
    XCTAssertTrue([[temps objectForKey:kMaximumTemperature] integerValue] == 50);

}

- (void)test_minimumAndMaximumTemperatures_SameValues {
    
    MTTDailyWeatherInformation *daily = [[MTTDailyWeatherInformation alloc] init];
    MTTHourlyWeatherInformation *hourly = [[MTTHourlyWeatherInformation alloc] init];
    hourly.tempC = 50;
    MTTHourlyWeatherInformation *hourly1 = [[MTTHourlyWeatherInformation alloc] init];
    hourly1.tempC = 50;
    MTTHourlyWeatherInformation *hourly2 = [[MTTHourlyWeatherInformation alloc] init];
    hourly2.tempC = 50;
    NSArray *hourlies = @[hourly, hourly1, hourly2];
    daily.hourly = hourlies;
    
    NSDictionary *temps = [daily minimumAndMaximumTemperatures];
    
    XCTAssertTrue([[temps objectForKey:kMinimumTemperature] integerValue] == 50);
    XCTAssertTrue([[temps objectForKey:kMaximumTemperature] integerValue] == 50);
    
}

- (void)test_minimumAndMaximumTemperatures_EmptyArray {
    
    MTTDailyWeatherInformation *daily = [[MTTDailyWeatherInformation alloc] init];
    NSArray *hourlies = @[];
    daily.hourly = hourlies;
    
    NSDictionary *temps = [daily minimumAndMaximumTemperatures];
    
    XCTAssertTrue([[temps objectForKey:kMinimumTemperature] integerValue] == 0);
    XCTAssertTrue([[temps objectForKey:kMaximumTemperature] integerValue] == 0);
}

@end
