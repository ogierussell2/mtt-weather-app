//
//  MTTAPIOperationManagerTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "MTTAPIOperationManager.h"
#import "OCMock.h"

// models
#import "MTTWeatherResponse.h"
#import "MTTCurrentWeatherInformation.h"
#import "MTTRequest.h"
#import "MTTDailyWeatherInformation.h"

@interface MTTAPIOperationManagerTests : XCTestCase

@end

@implementation MTTAPIOperationManagerTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_init_ValidSerializer {
    
    MTTAPIOperationManager *api = [[MTTAPIOperationManager alloc] init];
    XCTAssertTrue([api.requestSerializer isKindOfClass:[AFJSONRequestSerializer class]]);
}

- (void)test_init_CorrectBaseURLSet {
    
    MTTAPIOperationManager *api = [[MTTAPIOperationManager alloc] init];

    XCTAssertTrue([api.baseURL.absoluteString isEqualToString:@"http://api.worldweatheronline.com/"]);
    
}

- (void)test_getWeatherForCity_GETCalled {
    
    MTTAPIOperationManager *api = [[MTTAPIOperationManager alloc] init];
    id mockAPI = [OCMockObject partialMockForObject:api];
    
    [[[mockAPI expect] andForwardToRealObject] GET:[OCMArg any] parameters:[OCMArg any] success:[OCMArg any] failure:[OCMArg any]];
    OCMStub([mockAPI GET:[OCMArg any] parameters:[OCMArg any] success:[OCMArg any] failure:[OCMArg any]]);
    [api getWeatherForCity:@"city" withCompletionBlock:^(id weatherData) {} withFailureBlock:^(NSError *error) {}];
    
    [mockAPI verify];
    
}

- (void)test_getWeatherForCity_GETNotCalledForNilCity {
    
    MTTAPIOperationManager *api = [[MTTAPIOperationManager alloc] init];
    id mockAPI = [OCMockObject partialMockForObject:api];
    
    [[[mockAPI reject] andForwardToRealObject] GET:[OCMArg any] parameters:[OCMArg any] success:[OCMArg any] failure:[OCMArg any]];
    [api getWeatherForCity:nil withCompletionBlock:^(id weatherData) {} withFailureBlock:^(NSError *error) {}];
    
    [mockAPI verify];
}

- (void)test_getWeatherForCity_TestResponseMapsToObject {
    NSDictionary *responseDictionary = [self dictionaryFromJsonFile];
    
    MTTAPIOperationManager *api = [[MTTAPIOperationManager alloc] init];
    id mockAPI = [OCMockObject partialMockForObject:api];
    
    [[[[mockAPI expect] andForwardToRealObject] andDo:^(NSInvocation *invocation) {
        void (^successBlock)(id responseData) = nil;
        [invocation getArgument:&successBlock atIndex:3];
        successBlock(responseDictionary);
    }] getWeatherForCity:[OCMArg any] withCompletionBlock:[OCMArg any] withFailureBlock:[OCMArg any]];
    [[mockAPI stub] GET:[OCMArg any] parameters:[OCMArg any] success:[OCMArg any] failure:[OCMArg any]];
    
    __block MTTWeatherResponse *response = nil;
    [api getWeatherForCity:@"city" withCompletionBlock:^(id weatherData) {
        response = [[MTTWeatherResponse alloc] initWithData:responseDictionary];
    } withFailureBlock:^(NSError *error) {
    }];
    
    [self validateResponse:response];
    
}

#pragma mark - Helpers

- (BOOL)validateResponse:(MTTWeatherResponse *)response {
    XCTAssertNotNil(response);
    
    MTTCurrentWeatherInformation *currentInformation = response.currentCondition;
    
    XCTAssertTrue([currentInformation.observation_time isEqualToString:@"05:42 PM"]);
    
    MTTRequest *request = response.request;
    
    XCTAssertTrue([request.city isEqualToString:@"London"]);
    
    XCTAssertTrue(response.days.count == 5);
    XCTAssertTrue([[response.days firstObject] isKindOfClass:[MTTDailyWeatherInformation class]]);
    
    MTTDailyWeatherInformation *daily = [response.days firstObject];
    XCTAssertTrue(daily.hourly.count == 8);
    XCTAssertTrue([[daily.hourly firstObject] isKindOfClass:[MTTWeatherInformation class]]);
    XCTAssertTrue([daily.date isEqualToString:@"2015-07-03"]);
    XCTAssertNotNil(daily.astronomy);
    
    MTTHourlyWeatherInformation *hourly = [daily.hourly firstObject];
    XCTAssertNotNil(hourly);
    XCTAssertTrue(hourly.chanceofsunshine == 91);
    
    MTTAstronomy *astromony = daily.astronomy;
    XCTAssertNotNil(astromony);
    XCTAssertTrue([astromony.moonrise isEqualToString:@"09:58 PM"]);
    
    // would continue to test all properties of these objects given more time
    
}

- (NSDictionary *)dictionaryFromJsonFile {
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:NSStringFromClass(self.class) ofType:@"json"];
    NSError *error;
    return [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:path] options:0 error:&error];
}

@end
