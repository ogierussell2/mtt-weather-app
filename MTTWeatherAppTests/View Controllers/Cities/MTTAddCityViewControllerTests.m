//
//  MTTAddCityViewControllerTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "MTTCitiesViewController.h"
#import "MTTAddCityViewController.h"
#import "MTTAddCityDelegate.h"

@interface MTTAddCityViewController (Testing)

@property (nonatomic, assign) id<MTTAddCityDelegate> addCityDelegate;

@end

@interface MTTAddCityViewControllerTests : XCTestCase

@end

@implementation MTTAddCityViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_setDelegate_PropertySet {
    MTTAddCityViewController *controller = [[MTTAddCityViewController alloc] init];
    MTTCitiesViewController *citiesController = [[MTTCitiesViewController alloc] init];
    [controller setDelegate:(id<MTTAddCityDelegate>)citiesController];
    XCTAssertTrue([controller.addCityDelegate isEqual:citiesController]);
}

- (void)test_setDelegate_CitiesControllerRespondsToSelector {
    
    MTTAddCityViewController *controller = [[MTTAddCityViewController alloc] init];
    MTTCitiesViewController *citiesController = [[MTTCitiesViewController alloc] init];
    [controller setDelegate:(id<MTTAddCityDelegate>)citiesController];
    XCTAssertTrue([citiesController respondsToSelector:@selector(addCityData:)]);
}

@end
