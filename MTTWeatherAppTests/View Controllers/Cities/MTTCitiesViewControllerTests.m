//
//  MTTCitiesViewControllerTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "OCMock.h"
#import "MTTCitiesViewController.h"
#import "MTTWeatherForecastViewController.h"

@interface MTTCitiesViewController (Testing)

- (void)registerCells;
- (void)scrollView:(UIScrollView *)scrollView didSelectCellAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface MTTCitiesViewControllerTests : XCTestCase

@end

@implementation MTTCitiesViewControllerTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_viewDidLoad_CellsRegistered {
    
    MTTCitiesViewController *controller = [[MTTCitiesViewController alloc] init];
    id mockController = [OCMockObject partialMockForObject:controller];
    [[[mockController expect] andForwardToRealObject] registerCells];
    [controller viewDidLoad];
    
    [mockController verify];
}

@end
