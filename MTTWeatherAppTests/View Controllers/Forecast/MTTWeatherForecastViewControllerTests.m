//
//  MTTWeatherForecastViewControllerTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "OCMock.h"
#import "MTTWeatherForecastViewController.h"

@interface MTTWeatherForecastViewController (Testing)

@property (nonatomic, strong) MTTCityData *cityData;

- (void)registerCells;

@end

@interface MTTWeatherForecastViewControllerTests : XCTestCase

@end

@implementation MTTWeatherForecastViewControllerTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_viewDidLoad_CellsRegistered {
    
    MTTWeatherForecastViewController *controller = [[MTTWeatherForecastViewController alloc] init];
    id mockController = [OCMockObject partialMockForObject:controller];
    [[[mockController expect] andForwardToRealObject] registerCells];
    [controller viewDidLoad];
    
    [mockController verify];
}

- (void)test_setData_PropertySet {
    MTTWeatherForecastViewController *controller = [[MTTWeatherForecastViewController alloc] init];
    MTTCityData *cityData = MTTCityData.new;
    cityData.city = @"london";
    [controller setData:cityData];
    
    XCTAssertNotNil(controller.cityData);
    XCTAssertTrue([controller.cityData.city isEqualToString:@"london"]);
}

- (void)test_setData_NilPropertyDoesNotCrash {
    
    MTTWeatherForecastViewController *controller = [[MTTWeatherForecastViewController alloc] init];
    [controller setData:nil];
    
    XCTAssertNil(controller.cityData);
}

@end
