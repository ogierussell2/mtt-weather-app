//
//  MTTViewControllerTests.m
//  MTTWeatherApp
//
//  Created by Liam (ogie) Russell on 04/07/2015.
//  Copyright (c) 2015 Liam (ogie) Russell. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "MTTViewController.h"
#import "MTTAddCityViewController.h"

@interface MTTViewControllerTests : XCTestCase

@end

@implementation MTTViewControllerTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_segueIdentifier_CorrectIdentifier {
    
    XCTAssertTrue([[MTTViewController segueIdentifier] isEqualToString:@"MTTViewController"]);
}

- (void)test_segueIdentifier_CorrectIdentifierForSubclass {
    
    XCTAssertTrue([[MTTAddCityViewController segueIdentifier] isEqualToString:@"MTTAddCityViewController"]);
}

@end
